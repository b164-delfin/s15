console.log("Hello World!")

//Assignment Operator

let assignNumber = 8;

assignNumber = assignNumber + 2;
console.log(assignNumber);

assignNumber += 2;
console.log(assignNumber);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

let z = 1;
//increment

++z;
console.log(z);

z++;
console.log(z);
console.log(z++);
console.log(z);

// prefix vs post-fix incrementation 
console.log(z++); // 4
console.log(z); //5

console.log(++z); //6 - the new value is returned immediately.

// prefix and postfix decrementation
console.log(--z); // 5 - with prefix dec the result of subtraction by 1 is returned immediately

console.log(z--); //5 not returned immediately, previous value is returned first 
console.log(z); //4 

//Type Coercion
let numA = 10;
let numB = 12;


let numE = true + 1;
console.log(typeof numE)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'JUAN'); // false, JS is case sensitive
console.log(1 != 1); 
console.log(1 != 2); 
console.log(1 != '1'); 
console.log(0 != false); 
console.log('juan' != 'JUAN'); 

//Strictly equality operator 
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'JUAN'); // false, JS is case sensitive



//create login function

function login(username, password) {
	//check if the argument passed are strings

	if(typeof username === "string" && typeof password === "string") {
		console.log("Both Arguments are strings.") 
		/*
			nested if-else
			will run if the parent if statement is able to agree to accomplish its accomplish


		*/  
		if(username.length >= 8 && password.length >= 8 ) 
		{
			console.log("Thank you for logging in")
		}
		else if(username.length <= 8){
					console.log("Username is too short")
				} else if(password.length <= 8){
					console.log("password is too short")
				}

		 else{
			console.log("Credentials too short.")
		}
	} else {
		console.log("One of the arguments is not a string")
	}
}

login("jane", "jane123")


let message = 'No message.';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed < 30) {
		return 'Not a typhoon yet.';
	}
	else if(windSpeed <= 61) {
		return 'Tropical depression detected.';
	}
	else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.';
	}
	else if(windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected.';
	}
	else {
		return 'Typhoon detected.';
	}
}

message = determineTyphoonIntensity(68);
console.log(message);


let hero =prompt("Type a Hero:");

switch (hero) {
	case "Jose Rizal":
	console.log("National Hero of the Philippines");
	break;
	case "George Washington":
	console.log("Hero of the American Revolution");
	break;
	case "Hercules":
	alert("Legendary Hero of the Greek");
	break;
}